#!/bin/sh
if [ $1 = "production" ]
then
    echo "production" 
    sed -i -r "s/.*url.*/ url: $DESTINY_CARDS_API,/g" public/js/main.js
else
    echo "staging"
    sed -i -r "s/.*url.*/ url: $DESTINY_CARDS_STAGING_API,/g" public/staging/js/main.js
fi